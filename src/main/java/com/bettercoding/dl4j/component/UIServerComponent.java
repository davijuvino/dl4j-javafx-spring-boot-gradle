package com.bettercoding.dl4j.component;

import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UIServerComponent implements InitializingBean {
    private UIServer uiServer;
    private StatsStorage statsStorage;
    private StatsListener statsListener;
    private MultiLayerNetwork currentNetwork;

    @Value("${use-ui}")
    private boolean useUI;

    public void reinitialize(MultiLayerNetwork multiLayerNetwork) {
        if (useUI) {
            if (currentNetwork != null) {
                currentNetwork.getListeners().remove(statsListener);
            }
            if (multiLayerNetwork != null) {
                multiLayerNetwork.addListeners(statsListener);
            }
            currentNetwork = multiLayerNetwork;
        }
    }

    @Override
    public void afterPropertiesSet() {
        if (useUI) {
            statsStorage = new InMemoryStatsStorage();
            statsListener = new StatsListener(statsStorage);
            uiServer = UIServer.getInstance();
            uiServer.attach(statsStorage);
        }
    }

    public void stop() {
        if (useUI) {
            uiServer.stop();
        }
    }
}
