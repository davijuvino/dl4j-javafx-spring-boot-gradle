package com.bettercoding.dl4j.controller;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.springframework.stereotype.Component;

@Component
public class CustomLearningGuiControllerImpl implements CustomLearningGuiController {

    @Override
    public void onRefreshGUI() {

    }

    @Override
    public void onInitialize() {

    }

    @Override
    public void onTrainLoop(long loopNo) {

    }

    @Override
    public void onTestAction() {

    }

    @Override
    public void onTrainAction() {

    }

    @Override
    public MultiLayerNetwork onGetNeuralNetwork() {
        return null;
    }

    @Override
    public void onSetNeuralNetwork(MultiLayerNetwork restoreMultiLayerNetwork) {

    }
}
